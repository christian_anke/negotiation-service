package com.xommaterials.negotiationservice.controller;

import lombok.RequiredArgsConstructor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import org.springframework.web.client.RestTemplate;

@RestController
@RequiredArgsConstructor
@Slf4j
public class AuthorizationController {

    private String eProcurementServer;
    private RestTemplate restTemplate;

    @Autowired
    public AuthorizationController(
            @Value("${eProcurementServer}") String eProcurementServer,
            RestTemplate restTemplate) {
        this.eProcurementServer = eProcurementServer;
        this.restTemplate = restTemplate;
    }

    @GetMapping("/dummy/{rfqId}")
    public ResponseEntity<String> authorize(HttpServletRequest request, @PathVariable final long rfqId) {
        String authorization = request.getHeader(HttpHeaders.AUTHORIZATION);

        //Set the headers you need send
        final HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, authorization);

        //Create a new HttpEntity
        final HttpEntity<String> entity = new HttpEntity<>(headers);

        // call eProc endpoint to authorize here
        ResponseEntity<String> response = restTemplate.exchange(
                eProcurementServer + "/api/whoami",
                HttpMethod.GET,
                entity,
                String.class);

        log.info(response.toString());
        return ResponseEntity.ok(response.getBody());
    }
}
