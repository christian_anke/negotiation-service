package com.xommaterials.negotiationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableScheduling
public class Application {

    public static void main(final String[] args) {
        new SpringApplication(Application.class).run();
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
