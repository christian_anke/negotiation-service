# OPS readme

This document is required by OPS in order to deploy the application.


* Klaus
* Christian






The application itself is configured through the configuration file
`conf/conf.json`


This section will be updated. 



Since OPS couldn't provide auto-generated environment descriptors at the time
of this writing, all parties have agreed that `environment.json` and 
files referenced from there are part of this repository for the time being.
`environment.json` resides in `xom_env` folder of the application's project
and can be used from there during development and should be copied to its
target location following the specification. The same goes for files 
referenced from this file. Values in all environment descriptors need adaption
when deploying the application.
